const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    async create(data) {
        if (await UserRepository.getOne({ email: data.email })) {
            throw "Email is already taken"                          
        }
        if (await UserRepository.getOne({ phoneNumber: data.phoneNumber })) {
            throw "PhoneNumber is already taken"
        }
       return await UserRepository.create(data)
    }

    async getAll() {
        return await UserRepository.getAll();
    }

    async getById(id) {
        const user = await UserRepository.getOne({ userId: id });
         if (!user) throw 'User not found';
        return user
    }

    async update(id, dataToUpdate) {
        const oldUserdata = await UserRepository.getOne({ userId: id });
        if (!oldUserdata) throw 'Bad request';
        const newUserdata = { ...oldUserdata, ...dataToUpdate };
        return await UserRepository.update(id, newUserdata);
    }

    async delete(id) {
        const user = await UserRepository.getOne({ userId: id });
         if (!user) throw 'User not found';
        return await UserRepository.deleteUserId(id)
    }


    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();