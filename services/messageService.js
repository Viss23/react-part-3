const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {

    async create(data) {
       return await MessageRepository.create(data)
    }

    async getAll() {
        return await MessageRepository.getAll();
    }

    async getById(id) {
        const message = await MessageRepository.getOne({ id: id });
        if (!message) throw 'Message not found';
        return message
    }

    async update(id, dataToUpdate) {
        const oldMessageData = await MessageRepository.getOne({ id: id });
        if (!oldMessageData) throw 'Bad request';
        const newMessageData = { ...oldMessageData, ...dataToUpdate };
        return await MessageRepository.update(id, newMessageData);
    }

    async delete(id) {
        const message = await MessageRepository.getOne({ id: id });
        if (!message) throw 'Message not found';
        return await MessageRepository.delete(id)
    }

}

module.exports = new MessageService();