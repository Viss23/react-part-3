function filterObj(obj,arrayOfKeys){
  const filteredObj={}
  for (let [key, value] of Object.entries(obj)) {
      if (arrayOfKeys.includes(key)){
          filteredObj[key]=value;
      }
    }
  return filteredObj
}

function isPropertiesExistsInObj(arr,obj){
  for(let i=0;i<arr.length;i++){
    if ( !obj.hasOwnProperty(arr[i])){
      return false
    }
  }
  return true
}

function isEmpty(obj) {
  return !obj || Object.keys(obj).length === 0;
}

exports.filterObj = filterObj;
exports.isPropertiesExistsInObj = isPropertiesExistsInObj;
exports.isEmpty = isEmpty;