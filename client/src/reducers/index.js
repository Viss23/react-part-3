import { combineReducers } from 'redux';
import messages  from '../components/chat/reducer';
import editModal from '../components/chat/editMessage/reducer';
import addMessage from '../components/chat/addMessage/reducer';
import access from '../components/reducer';
import users from '../components/users/reducer';
import userPage from '../components/userPage/reducer'

const rootReducer = combineReducers({
  messages,
  editModal,
  addMessage,
  access,
  users,
  userPage
});

export default rootReducer;