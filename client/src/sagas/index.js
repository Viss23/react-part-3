import { all } from 'redux-saga/effects';
import  authSagas  from '../components/saga';
import  usersSagas from '../components/users/sagas';
import  userPageSagas from '../components/userPage/sagas'
import chatSagas from '../components/chat/saga';


export default function* rootSaga() {
  yield all([
    authSagas(),
    usersSagas(),
    userPageSagas(),
    chatSagas()
  ])
}