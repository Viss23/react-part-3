import React from 'react';
import Chat from './components/chat/Chat';
import Header from './components/common/Header';
import Footer from './components/common/Footer';
import EditMessage from './components/chat/editMessage'
import LoginPage from './components/loginPage';
import {  Switch,Route } from 'react-router-dom';
import UserRoute from './components/private/UserRoute';
import UserList from './components/users';
import UserPage from './components/userPage';
import AdminRoute from './components/private/AdminRoute';


function App() {
  return (
    <div>
      <Header />
      <Switch>
        <UserRoute  path="/chat" component={Chat} />
        <Route path="/editMessage" component={EditMessage} />
        <AdminRoute exact path="/users" component={UserList} />
        <AdminRoute path="/user/:id" component={UserPage} />
        <AdminRoute exact path="/user" component={UserPage} />
        <Route path="/" component={LoginPage} />
      </Switch>
      <Footer />
    </div>
  );
}



export default App;
