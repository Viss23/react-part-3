import { SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID, UPDATE_TEXT, RESET_TEXT } from "./actionTypes";

export const setCurrentMessageId = id => ({
    type: SET_CURRENT_MESSAGE_ID,
    payload: {
        id
    }
});

export const dropCurrentMessageId = () => ({
    type: DROP_CURRENT_MESSAGE_ID
});


export const updateText = (text) => ({
    type: UPDATE_TEXT,
    payload: {
        text
    }
})

export const resetText = () => ({
    type: RESET_TEXT
})