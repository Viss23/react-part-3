import { SET_CURRENT_MESSAGE_ID, DROP_CURRENT_MESSAGE_ID, UPDATE_TEXT, RESET_TEXT } from "./actionTypes";

const initialState = {
    messageId: '',
    text: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_MESSAGE_ID: {
            const { id } = action.payload;
            return {
                ...state,
                messageId: id
            };
        }
        case DROP_CURRENT_MESSAGE_ID: {
            return {
                ...state,
                messageId: ''
            };
        }

        case UPDATE_TEXT: {
            const { text } = action.payload;
            return {
                ...state,
                text
            };
        }

        case RESET_TEXT: {
            return {
                ...state,
                text: ''
            };
        }

        default:
            return state;
    }
}