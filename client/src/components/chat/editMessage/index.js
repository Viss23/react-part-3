import React from "react";
import { connect } from 'react-redux';
import * as actions from './actions';
import { updateMessage } from '../actions';
import style from '../../../mystyle.module.css';
import { withRouter } from "react-router";

class EditMessage extends React.Component {
  constructor(props) {
    super(props);
    this.onTextChange = this.onTextChange.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  onTextChange(text) {
    this.props.updateText(text);
  }

  onCancel() {
    this.props.dropCurrentMessageId();
    this.props.resetText();
    this.props.history.push('/chat');
  }

  onSave() {
    this.props.updateMessage(this.props.messageId, this.props.text);
    this.props.resetText();
    
    this.props.history.push('/chat');
  }

  getPageContent() {

    return (
    
          <div className={style.editMessage} >
            <div >
              <h5 className="modal-title">Edit message</h5>
            </div>
            <div className="modal-body">
              <textarea value={this.props.text} className={style.textAreaSendModal} onChange={(e) => this.onTextChange(e.target.value)}></textarea>
            </div>
            <div className="modal-footer">
              <button className="btn btn-secondary" onClick={this.onCancel}>Cancel</button>
              <button className="btn btn-primary" onClick={this.onSave}>Save</button>
            </div>
          </div> 
    )
  }

  render() {
   
    return  this.getPageContent() 
  }
}

const mapStateToProps = (state) => {
  return {
    messageId: state.editModal.messageId,
    text: state.editModal.text
  }
};

const mapDispatchToProps = {
  ...actions,
  updateMessage
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditMessage));