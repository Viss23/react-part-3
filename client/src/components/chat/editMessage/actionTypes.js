export const SET_CURRENT_MESSAGE_ID = "SET_CURRENT_MESSAGE_ID";
export const DROP_CURRENT_MESSAGE_ID = "DROP_CURRENT_MESSAGE_ID";
export const UPDATE_TEXT = "UPDATE_TEXT";
export const RESET_TEXT = "RESET_TEXT";