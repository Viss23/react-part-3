import axios from 'axios';
import api from '../../shared/config/api'; 
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES,FETCH_MESSAGES_SUCCESS,LOADING } from "./actionTypes";
import service from './service';

export function* fetchMessages() {
	try {
    yield put({type:LOADING});
    const messages = yield call(axios.get, `${api.url}/api/messages`);
     service.sortObjByCreatedAt(messages.data);
		yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { data: messages.data } })
	} catch (error) {
		console.log('fetchUsers error:', error.message)
	}
}

function* watchFetchMessages() {
	yield takeEvery(FETCH_MESSAGES, fetchMessages)
}

export function* addMessage(action) {
  const newMessage= {...action.payload};

  try {
    yield call(axios.post, `${api.url}/api/messages`, newMessage);
    yield put({type: FETCH_MESSAGES})
  } catch (error){
    console.log('Create error:',error.message);
  }
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* deleteMessage(action){
  try{
    yield call(axios.delete,`${api.url}/api/messages/${action.payload.id}`);
    yield put({type: FETCH_MESSAGES})
  } catch (error){
    console.log('Delete error:',error.message);
  }
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export function* updateMessage(action){
  const updatedMessage = {...action.payload}

  try{
    yield call(axios.put, `${api.url}/api/messages/${action.payload.id}`, updatedMessage);
    yield put({ type: FETCH_MESSAGES})
  } catch (error){
    console.log('Update error:',error.message);
  }
}

function* watchUpdateMessage() {
	yield takeEvery(UPDATE_MESSAGE, updateMessage)
}


export default function* chatSagas() {
	yield all([
		watchFetchMessages(),
		watchAddMessage(),
    watchDeleteMessage(),
    watchUpdateMessage()
	])
};