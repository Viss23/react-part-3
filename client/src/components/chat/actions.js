import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES } from './actionTypes';
import service from './service';

export const addMessage = data => ({
  type: ADD_MESSAGE,
  payload: {
    id: service.getNewId(),
    createdAt: service.getTime(),
    editedAt: '',
    ...data
  }
})

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
})

export const updateMessage = (id, text) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    editedAt: service.getTime(),
    text
  }
})

export const fetchMessages = () => ({
  type: FETCH_MESSAGES
})