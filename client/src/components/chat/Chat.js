import React from 'react';
import { connect } from 'react-redux'
import HeaderChat from './headerChat';
import MessageInput from './addMessage';
import MessageList from './messages/MessageList';
import { fetchMessages} from './actions';
import style from '../../mystyle.module.css';
import Loader from '../Loader.js';



class Chat extends React.Component {

  componentDidMount() {
      this.props.fetchMessages();
  }

  render() {
     if (this.props.loading) return <Loader /> 

    return (
      <div className={style.chat}>
        <HeaderChat />
        <MessageList />
        <MessageInput />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
   return {
    loading: state.messages.loading
  } 
};

const mapDispatchToProps = {
  fetchMessages
};


export default connect(mapStateToProps, mapDispatchToProps)(Chat);
