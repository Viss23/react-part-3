import { LOADING,FETCH_MESSAGES_SUCCESS, UPDATE_CURRENT_USER_INFO } from "./actionTypes";
import mockData from "../../data";
import service from './service';
import { FETCH_USER } from "../userPage/actionTypes";


const initialState = {
  loading: true,
  myUsername: "",
  myUserId: "",
  myAvatar: "",
  chatName: mockData.headerInfo.chatName,
  participantsAmount: mockData.headerInfo.participantsAmount,
  data: []
}

export default function (state = initialState, action) {
  switch (action.type) {

    case FETCH_MESSAGES_SUCCESS:{
      return {
        ...state,
        loading:false,
        data:action.payload.data
      }
    }
    
    case  UPDATE_CURRENT_USER_INFO:{
      const {userData} = action.payload;
      return {
        ...state,
        myUsername: userData.username,
        myUserId: userData.userId,
        myAvatar: userData.avatar,
      }
    }

    case LOADING:{
      return {
        ...state,
        loading:true
      }
    }

    default:
      return state
  }

}
