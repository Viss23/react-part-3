import React from 'react';
import style from '../../../mystyle.module.css';
import { connect } from 'react-redux';
import * as actions from './actions';
import { addMessage } from '../actions';

class AddMessage extends React.Component {
  constructor(props) {
    super(props);
    this.onTextChange = this.onTextChange.bind(this);
    this.onSendMessage = this.onSendMessage.bind(this);
  }

  onTextChange(text) {
    this.props.updateText(text);
  }

  onSendMessage() {
    const data = {
      text: this.props.text,
      user: this.props.myUsername,
      avatar: this.props.myAvatar,
      userId: this.props.myUserId,
    }
    this.props.addMessage(data);
    this.props.resetText();
  }

  render() {
    return (
      <div className={style.inputWrapper}>
        <textarea value={this.props.text} className={style.textAreaSend} onChange={(e) => this.onTextChange(e.target.value)}></textarea>
        <div className={style.sendBox} onClick={() => this.onSendMessage()} >
          <div className={style.sendSvg}></div>
          <span className={style.sendText}>Send</span>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    myUsername: state.messages.myUsername,
    myUserId: state.messages.myUserId,
    myAvatar: state.messages.myAvatar,
    text: state.addMessage.text,
  }
};

const mapDispatchToProps = {
  ...actions,
  addMessage
};


export default connect(mapStateToProps, mapDispatchToProps)(AddMessage);