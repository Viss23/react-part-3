import React from 'react';
import style from '../../../mystyle.module.css';
import moment from 'moment';

class MessageItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLiked: false
    }
    this.likeBtn = this.likeBtn.bind(this);
  }

  likeBtn() {
    this.setState({
      isLiked: !this.state.isLiked
    })
  }

  render() {
    const { myUserId, data, id } = this.props;
    const isMyMessage = myUserId === data.userId;
    const className = isMyMessage ? 'myMessage' : 'message';
    const onlyTime = moment(data.createdAt).format('LTS');
    const likeColor = this.state.isLiked ? 'deepskyblue' : 'grey';

    return (
      <div className={style[className]}>
        <div className={style.messageBox}>
          {!isMyMessage && <img className={style.avatar} src={data.avatar} alt='avatar'></img>}
          <p className={style.messageText}>{data.text}</p>
        </div>
        <div className={style.messageFooter}>
          {isMyMessage && <div className={style.delete} onClick={() => this.props.onDelete(id)}></div>}
          {isMyMessage && <div className={style.edit} onClick={() => this.props.onEdit(id, data.text)}></div>}
          <span>{onlyTime}</span>
          {!isMyMessage && <div style={{ backgroundColor: likeColor }} className={style.like} onClick={this.likeBtn}></div>}
        </div>
      </div>
    )
  }
}

export default MessageItem;