import React from 'react';
import Message from './MessageItem';
import style from '../../../mystyle.module.css';
import moment from 'moment';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { setCurrentMessageId, updateText } from '../editMessage/actions'
import { withRouter } from "react-router";

class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }


  editLastMessage() {
    const myLastMessage = this.props.data.find(message => message.userId === this.props.myUserId);
    if (myLastMessage === undefined) {
      alert("You didn't write anything")
    } else {
      this.props.setCurrentMessageId(myLastMessage.id);
      this.props.updateText(myLastMessage.text);
      this.props.history.push('/editMessage');
    }
  }

  handleKeyUp = (event) => {
    switch (event.key) {
      case 'ArrowUp':
        event.preventDefault();
        this.editLastMessage();
        break;
      default:
        break;
    }
  }

  componentWillMount() {
    document.addEventListener("keydown", this.handleKeyUp);
  }

  componentWillUnmount() {
    document.addEventListener("keydown", this.handleKeyUp);
  }

  onDelete(id) {
    this.props.deleteMessage(id);
  }

  onEdit(id, text) {

    this.props.setCurrentMessageId(id);
    this.props.updateText(text);
    this.props.history.push('/editMessage');
  }

  render() {

    const { data, myUserId } = this.props;
    const elements = [];
    let previousValue = 0;
    for (let i = 0; i < data.length; i++) {
      if (moment(data[i].createdAt).fromNow() !== previousValue) {
        elements.push(<span className={style.timeAgo} key={i}>{moment(data[i].createdAt).fromNow()}</span>)
      }
      previousValue = moment(data[i].createdAt).fromNow();
      elements.push(
        <Message
          myUserId={myUserId}
          data={data[i]}
          key={data[i].id}
          onDelete={this.onDelete}
          onEdit={this.onEdit}
          id={data[i].id} />
      )
    }

    return (
      <div className={style.messageList}>
        {elements}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.messages.data,
    myUserId: state.messages.myUserId,
  }
};

const mapDispatchToProps = {
  ...actions,
  setCurrentMessageId,
  updateText
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MessageList));