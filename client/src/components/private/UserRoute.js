import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from 'react-redux'

function UserRoute({ component: Component, isAuthenticated, isAdmin, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
            <Redirect to="/" />
          )
      }
    />
  );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.access.loggedIn,
  }
};

export default connect(mapStateToProps)(UserRoute);