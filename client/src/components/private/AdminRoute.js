import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from 'react-redux'

function AdminRoute({ component: Component, isAuthenticated, isAdmin, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        isAdmin ? (
          <Component {...props} />
        ) : (
            <Redirect to="/" />
          )
      }
    />
  );
}

const mapStateToProps = (state) => {
  return {
    isAdmin: state.access.isAdmin
  }
};

export default connect(mapStateToProps)(AdminRoute);