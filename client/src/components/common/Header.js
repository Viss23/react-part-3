import React from 'react';
import style from '../../mystyle.module.css';
import { BrowserRouter as Router, Link, Route } from "react-router-dom";


class Header extends React.Component {

  render() {

    return (
      <header>
        <div className={style.mark}></div>
        <div className={style.logo}></div>
        <div className={style.navigation}>
          <ul>
            <li>
              <Link to="/chat">Chat</Link>
            </li>
            <li>
              <Link to="/users"> User List</Link>
            </li>
          </ul>
        </div>
      </header>
    )
  }
}

export default Header;
