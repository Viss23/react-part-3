import React from 'react';
import { connect } from 'react-redux'
import { logIn, } from '../actions';
import style from '../../mystyle.module.css';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
    this.onLogIn = this.onLogIn.bind(this);
    this.onChangeData = this.onChangeData.bind(this);
  }

  onChangeData(e, type) {
    const value = e.target.value;
    this.setState({
      [type]: value
    })
  }

  onLogIn() {
    this.props.logIn({
      username: this.state.username,
      password: this.state.password
    })
  }


  render() {
    if (this.props.loggedIn && this.props.isAdmin) {
      this.props.history.push('/users');
    } else if (this.props.loggedIn) {
      this.props.history.push('/chat')
    }

    return (
      <div className={style.form}>
        <p>Log in</p>
        <label for="username">Username</label>
        <input id="username" type='text' text={this.state.username} onChange={(e) => this.onChangeData(e, 'username')}></input>
        <label for="pass">Password</label>
        <input id="pass" type='password' text={this.state.password} onChange={(e) => this.onChangeData(e, 'password')}></input>
        <button onClick={() => this.onLogIn()}>Log in</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ...state.access
  }
};

const mapDispatchToProps = {
  logIn
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
