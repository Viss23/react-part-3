import {LOG_OUT,AUTH} from './actionTypes';
 
export const logIn = data =>({
  type: AUTH,
  payload: {
    username: data.username,
    password: data.password
  }
})


export const logOut = () =>({
  type: LOG_OUT,
  payload: {
    id: '',
    loggedIn: false,
    isAdmin: false
  }
})