import axios from 'axios';
import api from '../shared/config/api.json';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { AUTH, AUTH_SUCCESS} from "./actionTypes";
import { UPDATE_CURRENT_USER_INFO} from "./chat/actionTypes";

export function* auth(action) {
    
    try {
        const user = yield call(axios.post, `${api.url}/api/auth/login`,{
          ...action.payload
        });
        yield put({ type: AUTH_SUCCESS, payload: { userData: user.data } })
        yield put({ type: UPDATE_CURRENT_USER_INFO,payload:{userData: user.data}})
    } catch (error) {
        console.log('Auth error:', error.message)
    }
}

function* watchAuth() {
    yield takeEvery(AUTH, auth)
}

export default function* authSagas() {
    yield all([
        watchAuth()
    ])
}