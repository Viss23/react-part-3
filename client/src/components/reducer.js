import { AUTH_SUCCESS,LOG_OUT} from './actionTypes';


const initialState ={
  loggedIn: false,
  isAdmin: false,
  userId: ''
}

export default function (state = initialState,action){
  switch (action.type) {

    case AUTH_SUCCESS: {
       const {userData} = action.payload;
       const isAdmin =userData.role === 'admin' ? true : false;
        
      return {
        loggedIn: true,
        userId: userData.userId,
        username: userData.username,
        isAdmin: isAdmin,
        avatar: userData.avatar
      }
    }

    case  LOG_OUT: {
        return {
          ...action.payload
        }
    }


    default:
      return state;
  }
}