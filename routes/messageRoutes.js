const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');
/* const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware'); */

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', getAll, responseMiddleware);//+
router.get('/:id', getById, responseMiddleware);//-?
router.post('/', create, responseMiddleware);//+
router.put('/:id', update, responseMiddleware);//+
router.delete('/:id', _delete, responseMiddleware)//+

function create(req, res, next) {
  MessageService.create(req.body)
    .then((result) => res.json(result))
    .catch(err => next(err));
}

function getAll(req, res, next) {
  MessageService.getAll()
    .then(messages => res.json(messages))
    .catch(err => next(err))
}

function getById(req, res, next) {
  MessageService.getById(req.params.id)
    .then((message) => res.json(message))
    .catch(err => next(err))
}

function update(req, res, next) {
  MessageService.update(req.params.id, req.body)
    .then((updatedMessage) => res.json(updatedMessage))
    .catch(err => next(err));
}

function _delete(req, res, next) {
  MessageService.delete(req.params.id)
    .then((deleted) => res.json(deleted))
    .catch(err => next(err));
}

module.exports = router;