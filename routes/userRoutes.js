const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/:id', getById, responseMiddleware); 
router.get('/', getAll, responseMiddleware);
router.post('/', register, responseMiddleware); 
router.put('/:id' , update, responseMiddleware) 
router.delete('/:id', _delete, responseMiddleware);

function register(req, res, next) {
  UserService.create(req.body)
    .then((result) => res.json(result))
    .catch(err => next(err));
}

function getAll(req, res, next) {
  UserService.getAll()
    .then(users => res.json(users))
    .catch(err => next(err))
}

function getById(req, res, next) {
  UserService.getById(req.params.id)
    .then((user) => res.json(user))
    .catch(err => next(err))
}

function update(req, res, next) {
  UserService.update(req.params.id, req.body)
    .then((updatedUser) => res.json(updatedUser))
    .catch(err => next(err));
}

function _delete(req, res, next) {
  UserService.delete(req.params.id)
    .then((deletedUser) => res.json(deletedUser))
    .catch(err => next(err));
}

module.exports = router;