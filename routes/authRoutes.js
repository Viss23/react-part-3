const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action
        const { username, password } = req.body;
        const user = AuthService.login({ username: username });
        if (user.password === password) {
            res.json(user);
        } else {
            throw 'Invalid password'
        }
    } catch (err) {
        next(err)
    }
    next();
}, responseMiddleware);

module.exports = router;