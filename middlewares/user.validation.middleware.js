const { user } = require('../models/user');
const { filterObj, isPropertiesExistsInObj, isEmpty } = require('../helpers/validationHelper');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        if (req.body.hasOwnProperty('id') ){    
            throw 'Id can not be in body'
        }

        const requiredFields = ['firstName', 'lastName', 'email', 'phoneNumber', 'password'];
        if (!isPropertiesExistsInObj(requiredFields, req.body)) {
            throw "Some properties don't exists"
        }

        req.body = filterObj(req.body, requiredFields);

        if (!isValidEmail(req.body.email)) {
            throw 'Invalid email'
        }
        if (!isValidPhoneNumber(req.body.phoneNumber)) {
            throw 'Invalid phonenumber'
        }
        if (!isValidPassword(req.body.password)) {
            throw 'Invalid password'
        }
    } catch (err) {
        next(err)
    } finally {
        next()
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        if (req.body.hasOwnProperty('id') ){    
            throw 'Id can not be in body'
        }

       
        if (isEmpty(req.body)) {
            throw 'Wrong properties'
        }
        if (req.body.hasOwnProperty('email') && !isValidEmail(req.body.email)) {
            throw 'Invalid email'
        }
        if (req.body.hasOwnProperty('phoneNumber') && !isValidPhoneNumber(req.body.phoneNumber)) {
            throw 'Invalid phonenumber'
        }
        if (req.body.hasOwnProperty('password') && !isValidPassword(req.body.password)) {
            throw 'Invalid password'
        }
    } catch (err) {
        next(err)
    } finally {
        next()
    }
}

function isValidEmail(email) {
    return email.endsWith('@gmail.com')
}

function isValidPhoneNumber(phoneNumber) {
    return (phoneNumber.startsWith('+380') && phoneNumber.length === 13)
}

function isValidPassword(password) {
    return (password.length >= 3)
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;