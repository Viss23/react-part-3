const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }

    deleteUserId(id){
        return this.dbContext.remove({ userId:id }).write();
    }

    update(id, dataToUpdate) {
        dataToUpdate.editedAt = new Date();
        return this.dbContext.find({ userId:id }).assign(dataToUpdate).write();
    }

    create(data) {
        data.userId = this.generateId();
        data.createdAt = new Date();
        const list = this.dbContext.push(data).write();
        return list.find(it => it.userId === data.userId);
    }
}

exports.UserRepository = new UserRepository();